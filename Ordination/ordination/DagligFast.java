package ordination;

import java.time.*;

public class DagligFast extends Ordination{
    private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient){
		super(startDen, slutDen, laegemiddel, patient);
	}

	@Override
	public double samletDosis() {
		double sSum = this.doegnDosis() * this.antalDage();
		return sSum;
	}
	
	@Override
	public double doegnDosis() {
		double sum = 0;
		for(Dosis d: doser){
			sum += d.getAntal();
		}
		return sum;
	}
	@Override
	public String getType() {
		//final String SKAEV = "SKAEV";
		return "FAST";
	}
	
	public Dosis[] getDoser(){
		return doser;
	}
	/**
	 * 
	 * @param i 0 morgen, 1 middag, 2 aften, 3 nat
	 * @return
	 */
	public void addDoser(int i, double antal){
		LocalTime timeOfDay = null;
		if(i == 0){timeOfDay.of(6, 0);}
		else if(i== 1){timeOfDay.of(12, 0);}
		else if(i== 2){timeOfDay.of(18, 0);}
		else if(i== 3){timeOfDay.of(23, 59);}
		else{timeOfDay.of(0, 0);}
		doser[i] = new Dosis(timeOfDay, antal);
	}
	
	public int ordStat(){
		return 4 * this.antalDage();
	}

}

