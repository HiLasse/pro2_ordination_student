package ordination;

import java.time.LocalTime;
import java.util.ArrayList;
import java.time.LocalDate;
public class DagligSkaev extends Ordination{
    private ArrayList<Dosis> doser = new ArrayList<Dosis>();

	public DagligSkaev(LocalDate startDen,LocalDate slutDen, Laegemiddel laegemiddel, Patient patient){
		super(startDen, slutDen, laegemiddel, patient);
	}
	
    public void opretDosis(LocalTime tid, double antal) {
    	doser.add(new Dosis(tid, antal));
    	
    }
	@Override
	public double samletDosis() {
		return (this.doegnDosis() * this.antalDage());
	}
	
	@Override
	public double doegnDosis() {
		double sum = 0;
		for(Dosis d: doser){
			sum += d.getAntal();
		}
		return sum;
	}
	@Override
	public String getType() {
		//final String SKAEV = "SKAEV";
		return "SKAEV";
	}
	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}
	
	public int ordStat(){
		return this.doser.size() * this.antalDage();
	}

}
