package ordination;

import java.util.ArrayList;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;
	private ArrayList<Ordination> ordinationer = new ArrayList<Ordination>();

	// TODO: Link til Ordination

	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
	}

	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt(){
		return vaegt;
	}

	public void setVaegt(double vaegt){
		this.vaegt = vaegt;
	}

	//TODO: Metoder (med specifikation) til at vedligeholde link til Ordination

	@Override
	public String toString(){
		return navn + "  " + cprnr;
	}
	public void addOrdination(Ordination ordination){
		this.ordinationer.add(ordination);
	}

	public ArrayList<Ordination> getOrdinationer() {
		// TODO Auto-generated method stub
		return new ArrayList<Ordination>(ordinationer);
	}

	public int ordStat(Laegemiddel laegemiddel){
		int ord = 0;
		for(Ordination o: ordinationer){
			if(o.getLaegemiddel() == laegemiddel){
				ord += o.ordStat();
			}
		}
		return ord;
	}

}
