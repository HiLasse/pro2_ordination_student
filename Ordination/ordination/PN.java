package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

import ordination.*;

public class PN extends Ordination{



	private double antalEnheder;
	private ArrayList<LocalDate> gives = new ArrayList<LocalDate>();
	private LocalDate dagsDato;


	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen
	 * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * @param givesDen
	 * @return
	 */
	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder, Patient patient) {
		super(startDen, slutDen, laegemiddel, patient);
		this.antalEnheder = antalEnheder;
	}

	public boolean givDosis(LocalDate givesDen) {
		gives.add(givesDen);
		dagsDato = givesDen;
		return false;   
	}

	public double doegnDosis() {
		int i = 0;
		for(LocalDate d: gives){
			if(d.equals(dagsDato)){
				i++;
			}
		}
		return i * antalEnheder;
	}


	public double samletDosis() {
		// TODO
		return gives.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * @return
	 */
	public int getAntalGangeGivet() {
		// TODO
		return gives.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "PN";
	}
	
	public int ordStat(){
		return this.getAntalGangeGivet();
	}

}
