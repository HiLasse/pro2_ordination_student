package service;

import ordination.*;
import storage.Storage;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Service {

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 * @return opretter og returnerer en PN ordination.
	 */
	public static PN opretPNOrdination(LocalDate startDen, LocalDate slutDen,
			Patient patient, Laegemiddel laegemiddel, double antal){
		if(checkStartFoerSlut(startDen, slutDen) == false){
			try {
				throw new DateException("Start date after End date");
			} catch (DateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		PN pn = new PN(startDen, slutDen, laegemiddel, antal, patient);
		return pn;
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. 
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og 
	 * ordinationen oprettes ikke
	 */
	public static DagligFast opretDagligFastOrdination(LocalDate startDen,
			LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal){
		if(checkStartFoerSlut(startDen, slutDen) == false){
			try {
				throw new DateException("Start date after End date");
			} catch (DateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		DagligFast dagligFast = new DagligFast(startDen, slutDen, laegemiddel, patient);
		dagligFast.addDoser(0, morgenAntal);
		dagligFast.addDoser(1, middagAntal);
		dagligFast.addDoser(2, aftenAntal);
		dagligFast.addDoser(3, natAntal);
		// TODO
		return dagligFast;
	}

	/**
	 *  Opretter og returnerer en DagligSkæv ordination.
	 *  Hvis startDato er efter slutDato kastes en IllegalArgumentException og 
	 *  ordinationen oprettes ikke
	 * @throws DateException 
	 */
	public static DagligSkaev opretDagligSkaevOrdination(LocalDate startDen,
			LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			LocalTime[] klokkeSlet, double[] antalEnheder){
		if(checkStartFoerSlut(startDen, slutDen) == false){
			try {
				throw new DateException("Start date after End date");
			} catch (DateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		DagligSkaev dagligSkaev = new DagligSkaev(slutDen, slutDen, laegemiddel, patient);
		for(int i = 0; i < klokkeSlet.length; i++){
			dagligSkaev.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		return dagligSkaev;
	}

	/**
	 *  En dato for hvornår ordinationen anvendes tilføjes
	 *  ordinationen. Hvis datoen ikke er indenfor ordinationens
	 *  gyldighedsperiode kastes en IllegalArgumentException
	 * @throws DateException 
	 */
	public static void ordinationPNAnvendt(PN ordination, LocalDate dato){
		if(checkTidsrum(ordination.getStartDen(), ordination.getSlutDen(), dato)){
			try {
				ordination.givDosis(dato);
				throw new DateException("Date out of range");
			} catch (DateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages
	 * hensyn til patientens vægt). Det er en forskellig enheds faktor
	 * der skal anvendes, og den er afhængig af patientens vægt.
	 */
	public static double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double result;
		if (patient.getVaegt() < 25) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		}
		else if (patient.getVaegt() > 120) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		}
		else {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return result;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af ordinationer.
	 */
	public static int antalOrdinationerPrVægtPrLægemiddel(double vægtStart,
			double vægtSlut, Laegemiddel laegemiddel) {
		int antal = 0;
		for(Patient p: Service.getAllPatienter()){
			if(p.getVaegt() < 25){
				antal += p.ordStat(laegemiddel);
			}
			if(p.getVaegt() > 120){
				antal += p.ordStat(laegemiddel);
			}
			
			if(p.getVaegt() > 25 && p.getVaegt() < 120){
				antal += p.ordStat(laegemiddel);
			}
		}
		return antal;
	}


	public static ArrayList<Patient> getAllPatienter() {
		return Storage.getAllPatienter();
	}

	public static ArrayList<Laegemiddel> getAllLaegemidler() {
		return Storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger før en slutDato.
	 * @return true hvis startDato er før slutDato, false ellers.
	 */
	public static boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		boolean result = true;
		if(DateException.isLocalDate(slutDato) == false || DateException.isLocalDate(startDato) == false ){
			result= false;
		}
		else if(slutDato.compareTo(startDato) < 0) {
			result = false;
		}
		return result;
	}
	/**
	 * Metode der kan bruges til at checke at en testDate ligger indenfor et tidsrum startDato, slutDato.
	 * @return true udenfor tidsrum, false ellers.
	 */
	public static boolean checkTidsrum(LocalDate startDato, LocalDate slutDato, LocalDate testDate){
		if(0 > slutDato.compareTo(testDate) || 0 < startDato.compareTo(testDate)){
			return true;
		}
		else{
			return false;
		}
	}

	public static void createSomeObjects() {
		Storage.gemPatient(new Patient("Jane Jensen", "121256-0512", 63.4));
		Storage.gemPatient(new Patient("Finn Madsen", "070985-1153", 83.2));
		Storage.gemPatient(new Patient("Hans Jørgensen", "050972-1233", 89.4));
		Storage.gemPatient(new Patient("Ulla Nielsen", "011064-1522", 59.9));
		Storage.gemPatient(new Patient("Ib Hansen", "090149-2529", 87.7));

		Storage.gemLaegemiddel(new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk"));
		Storage.gemLaegemiddel(new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml"));
		Storage.gemLaegemiddel(new Laegemiddel("Fucidin", 0.025, 0.025, 0.025,
				"Styk"));
		Storage.gemLaegemiddel(new Laegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk"));

		opretPNOrdination(LocalDate.of(2015, 1, 1),
				LocalDate.of(2015, 1, 12), Storage.getAllPatienter().get(0),
				Storage.getAllLaegemidler().get(1), 123);

		opretPNOrdination(LocalDate.of(2015, 2, 12),
				LocalDate.of(2015, 2, 14), Storage.getAllPatienter().get(0),
				Storage.getAllLaegemidler().get(0), 3);

		opretPNOrdination(LocalDate.of(2015, 1, 20),
				LocalDate.of(2015, 1, 25), Storage.getAllPatienter().get(3),
				Storage.getAllLaegemidler().get(2), 5);

		opretPNOrdination(LocalDate.of(2015, 1, 1),
				LocalDate.of(2015, 1, 12), Storage.getAllPatienter().get(0),
				Storage.getAllLaegemidler().get(1), 123);

		opretDagligFastOrdination(LocalDate.of(2015, 1, 10),
				LocalDate.of(2015, 1, 12), Storage.getAllPatienter().get(1),
				Storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23),
				LocalDate.of(2015, 1, 24), Storage.getAllPatienter().get(1),
				Storage.getAllLaegemidler().get(2), kl, an);
	}

}
