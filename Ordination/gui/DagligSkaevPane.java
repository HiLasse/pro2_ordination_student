package gui;

import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import ordination.DateException;

public class DagligSkaevPane extends GridPane {
	private TextField txtTimeMinut = new TextField();
	private TextField txtMaengde = new TextField();
	private Button btnOpret = new Button("Opret dosis");
	private ListView<String> listDosis = new ListView<>();
	private Label lblError = new Label();

	public DagligSkaevPane() {
		setHgap(20);
		setVgap(10);
		setGridLinesVisible(false);

		txtTimeMinut.setPromptText("TT:MM");
		txtMaengde.setPromptText("Mængde");

		lblError.setTextFill(Color.RED);
		add(lblError, 0, 8, 2, 1);

		HBox hbox = new HBox(8);
		hbox.getChildren().add(txtTimeMinut);
		hbox.getChildren().add(txtMaengde);
		hbox.getChildren().add(btnOpret);
		add(hbox, 0, 0);

		listDosis.setMaxHeight(100);
		add(listDosis, 0, 1);

		btnOpret.setOnAction(event -> opretDosis());
	}

	private void opretDosis() {
		if (DateException.isTime(txtTimeMinut.getText()) == false){
			lblError.setText("Ikke et klokkeslet");
		}
		else if (DateException.isDouble(txtMaengde.getText()) == false){
			lblError.setText("Ikke en mængde");
		}
		else if (Double.parseDouble(txtMaengde.getText()) <= 0) {
			lblError.setText("Dosis skal være et positivt tal");
		}
		else{
			String dosis = txtTimeMinut.getText() + " " + txtMaengde.getText();
			listDosis.getItems().add(dosis);}
	}

	public String[] getDosisArray() {
		ObservableList<String> items = listDosis.getItems();
		return items.toArray(new String[items.size()]);
	}
}
